//
//  MappedData.swift
//  UsabillaTest
//
//  Created by Renee Alves on 15/02/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation
import ObjectMapper

class MappedData: Mappable {
    
    var browser: String = ""
    var browserVersion: String = ""
    var platform: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var country: String = ""
    var rating: Int = 0
    var labels: [String] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        
        browser <- map["computed_browser.Browser"]
        browserVersion <- map["computed_browser.Version"]
        platform <- map["computed_browser.Platform"]
        latitude <- map["geo.lat"]
        longitude <- map["geo.lon"]
        country <- map["geo.country"]
        rating <- map["rating"]
        labels <- map["labels"]
    }
}
