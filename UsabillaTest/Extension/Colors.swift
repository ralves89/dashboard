//
//  Colors.swift
//  UsabillaTest
//
//  Created by Renee Alves on 15/02/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func whiteFive() -> UIColor{
        return UIColor(red: 216.0 / 255.0, green: 216.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
    }
}
