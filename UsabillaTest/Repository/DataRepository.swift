//
//  DataRepository.swift
//  UsabillaTest
//
//  Created by Renee Alves on 15/02/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation
import Alamofire

class DataRepository {
    
    let url = "http://cache.usabilla.com/example/apidemo.json"
    
    func getData(callbackHandler: @escaping ([MappedData])->()) {
        
        Alamofire.request(url)
            .responseJSON { (response) in
                
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    
                    if let items = json["items"] as? [Dictionary<String,AnyObject>] {
                        
                        var mappedData: [MappedData] = []
                        for item in items {
                            
                            guard let map = MappedData(JSON: item) else {continue}
                            mappedData.append(map)
                        }
                        callbackHandler(mappedData)
                    }
                }
        }
    }
}
