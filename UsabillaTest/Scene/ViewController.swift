//
//  ViewController.swift
//  UsabillaTest
//
//  Created by Renee Alves on 14/02/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var labelsProgressView: CircularProgressView!
    @IBOutlet weak var chromeProgressView: ProgressView!
    @IBOutlet weak var safariProgressView: ProgressView!
    @IBOutlet weak var firefoxProgressView: ProgressView!
    
    @IBOutlet weak var bugs: UILabel!
    @IBOutlet weak var suggestions: UILabel!
    @IBOutlet weak var questions: UILabel!
    
    @IBOutlet weak var country1: UILabel!
    @IBOutlet weak var country2: UILabel!
    @IBOutlet weak var country3: UILabel!
    @IBOutlet weak var country4: UILabel!
    @IBOutlet weak var country5: UILabel!
    
    @IBOutlet weak var country1Count: UILabel!
    @IBOutlet weak var country2Count: UILabel!
    @IBOutlet weak var country3Count: UILabel!
    @IBOutlet weak var country4Count: UILabel!
    @IBOutlet weak var country5Count: UILabel!
    
    var interactor: DashboardInteractor?
    var presenter: DashboardPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor = DashboardInteractor()
        presenter = DashboardPresenter()
        interactor?.presenter = presenter
        presenter?.view = self
        
        interactor?.fecthData()
    }
    
    func setupComplimentLabel(percentage: CGFloat) {
        
        self.labelsProgressView.animateProgressView(percentage)
    }
    
    func setup(labels: Dictionary<String,String>) {
        
        guard let bugsText = labels["bugs"] else {return}
        guard let questionsText = labels["questions"] else {return}
        guard let suggestionsText = labels["suggestions"] else {return}
        
        self.bugs.text = bugsText
        self.questions.text = questionsText
        self.suggestions.text = suggestionsText
    }
    
    func setup(countries: Dictionary<String,Int>) {
        
        let countriesLabels = [self.country1, self.country2, self.country3, self.country4, self.country5]
        let countriesCount = [self.country1Count, self.country2Count, self.country3Count, self.country4Count, self.country5Count]
        let sortedDict = countries.sorted(by: {$0.value > $1.value})
        
        for i in 0..<5 {
            
            guard let key = sortedDict[i].key as? String else {continue}
            guard let value = sortedDict[i].value as? Int else {continue}
            
            countriesLabels[i]?.text = key
            countriesCount[i]?.text = String(value)
        }
    }
    
    func setup(browsers: Dictionary<String,CGFloat>) {
        
        self.chromeProgressView.category = "Chrome"
        self.safariProgressView.category = "Safari"
        self.firefoxProgressView.category = "Firefox"
        
        guard let chromePercentage = browsers["Chrome"] else {return}
        guard let firefoxPercentage = browsers["Firefox"] else {return}
        guard let safariPercentage = browsers["Safari"] else {return}
        
        self.chromeProgressView.animateTo(maxValue: chromePercentage)
        self.safariProgressView.animateTo(maxValue: safariPercentage)
        self.firefoxProgressView.animateTo(maxValue: firefoxPercentage)
    }
}

