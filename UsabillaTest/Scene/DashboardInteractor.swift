//
//  DashboardInteractor.swift
//  UsabillaTest
//
//  Created by Renee Alves on 15/02/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation

protocol InteractorLogic {
    func fecthData()
}

class DashboardInteractor: InteractorLogic {
    
    var presenter: DashboardPresenter?
    
    func fecthData() {
    
        let repository = DataRepository()
        
        repository.getData { (mappedData) in
            
            self.presenter?.present(data: mappedData)
        }
    }
}

