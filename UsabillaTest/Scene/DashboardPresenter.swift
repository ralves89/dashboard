//
//  DashboardPresenter.swift
//  UsabillaTest
//
//  Created by Renee Alves on 15/02/19.
//  Copyright © 2019 Renee Alves. All rights reserved.
//

import Foundation
import UIKit

class DashboardPresenter {
    
    weak var view: ViewController?
    
    func present(data: [MappedData]) {
        
        var set: Set<String> = []
        var complimentCounter = 0
        var suggestionCounter = 0
        var questionCounter = 0
        var bugCounter = 0
        
        for item in data {
            
            if item.labels.contains("compliment") {complimentCounter = complimentCounter + 1}
            else if item.labels.contains("bug") {bugCounter += 1}
            else if item.labels.contains("question") {questionCounter += 1}
            else if item.labels.contains("suggestion") {suggestionCounter += 1}
            
            set.insert(item.country)
        }
        
        self.present(compliments: complimentCounter, total: data.count)
        self.present(countries: set, basedOn: data)
        self.present(bugs: bugCounter,
                     questions: questionCounter,
                     suggestions: suggestionCounter,
                     basedOn: data.count)
        self.presentBrowsersWith(data: data)
    }
    
    private func present(compliments: Int, total: Int) {
        
        let percentage = CGFloat(compliments) / CGFloat(total)
        self.view?.setupComplimentLabel(percentage: percentage)
    }
    
    private func present(bugs: Int,
                         questions: Int,
                         suggestions: Int,
                         basedOn total: Int) {
        
        var labelsDictionary: Dictionary<String,String> = [:]
        let bugsPercentage = (CGFloat(bugs) / CGFloat(total)) * 100
        let questionsPercentage = (CGFloat(questions) / CGFloat(total)) * 100
        let suggestionsPercentage = (CGFloat(suggestions) / CGFloat(total)) * 100
        
        labelsDictionary["bugs"] = "\(String(format: "%.1f", bugsPercentage))%"
        labelsDictionary["suggestions"] = "\(String(format: "%.1f", suggestionsPercentage))%"
        labelsDictionary["questions"] = "\(String(format: "%.1f", questionsPercentage))%"
        
        self.view?.setup(labels: labelsDictionary)
    }
    
    private func present(countries: Set<String>, basedOn data: [MappedData]) {
        
        var countryDictionary: Dictionary<String,Int> = [:]
        for country in countries {
            
            var counter = 0
            for item in data {
                
                if item.country == country {
                    
                    counter = counter + 1
                    countryDictionary[country] = counter
                }
            }
        }
        self.view?.setup(countries: countryDictionary)
    }
    
    private func presentBrowsersWith(data: [MappedData]) {
        
        var browsers: Dictionary<String,CGFloat> = [:]
        
        var chromeCount = 0
        var safariCount = 0
        var firefoxCount = 0
        
        for item in data {
            
            if item.browser == "Chrome" {chromeCount = chromeCount + 1}
            else if item.browser == "Firefox" {firefoxCount = firefoxCount + 1}
            else if item.browser == "Safari" {safariCount = safariCount + 1}
        }
        
        browsers["Chrome"] = CGFloat(chromeCount) / CGFloat(data.count)
        browsers["Firefox"] = CGFloat(firefoxCount) / CGFloat(data.count)
        browsers["Safari"] = CGFloat(safariCount) / CGFloat(data.count)
        
        self.view?.setup(browsers: browsers)
    }
}
