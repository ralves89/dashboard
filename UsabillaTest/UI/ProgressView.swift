//
//  ProgressView.swift
//  NG
//
//  Created by Renee Alves on 18/12/17.
//  Copyright © 2017 Renee Alves. All rights reserved.
//

import Foundation
import UIKit

class ProgressView: UIView {
    
    var liquidView: UIView?
    var shapeView: UIView?
    var category: String?
    var min: Int?
    var max: Int?
    var mainGraphic: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init(frame: CGRect, progressCategory: String) {
        
        super.init(frame: frame)
        self.category = progressCategory
        setup()
    }
    
    func setup() {
        
        liquidView = UIView()
        shapeView = UIView()
        
        self.backgroundColor = UIColor.whiteFive()
        self.shapeView?.backgroundColor = UIColor.whiteFive()
        
        self.addSubview(liquidView!)
        //self.mask = shapeView
        self.layer.mask = roundedCornerFor(self, witdh: 80, height: 10)
        
        layoutIfNeeded()
        reset()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        liquidView?.frame = self.bounds
        shapeView?.frame = self.bounds
    }
    
    func reset() {
        liquidView?.frame.origin.x = 0 - bounds.width
    }
    
    func animateTo(maxValue: CGFloat) {
        
        let max = self.setupMaxValue(maxValue: maxValue)
        let offSet = self.getOffSetWith(max: max)
        
        reset()
        
        UIView.animate(withDuration: 0.1) {
            self.liquidView?.backgroundColor = self.setupLiquidViewColorBasedOn(maxValue: max, category: self.category!)
            self.liquidView?.frame.origin.x = -offSet
        }
    }
    
    private func getOffSetWith(max: CGFloat) -> CGFloat {
        
        let offSet = (liquidView?.frame.size.width)! - ((liquidView?.frame.size.width)! * max)
        
        return offSet
    }
    
    private func setupMaxValue(maxValue: CGFloat) -> CGFloat {
        
        var max: CGFloat = 0.0
        
        if maxValue > 1.0 {
            
            max = 1.0
        } else if maxValue < 0.0 {
            
            max = 0.0
        }  else {
            
            max = maxValue
        }
        
        return max
    }
    
    private func setupLiquidViewColorBasedOn (maxValue: CGFloat, category: String) -> UIColor {
        
        var color: UIColor!
        switch category {
        case "Chrome":
            color = UIColor.red
        case "Firefox":
            color = UIColor.orange
        case "Safari":
            color = UIColor.green
        default:
            color = UIColor.blue
        }
        
        return color
    }
}

func roundedCornerFor(_ view: UIView, witdh: CGFloat, height: CGFloat) -> CAShapeLayer {
    
    let roundedCorner = CAShapeLayer()
    roundedCorner.bounds = view.frame
    roundedCorner.position = view.center
    roundedCorner.path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.bottomRight , .topRight, .bottomLeft, .topLeft], cornerRadii: CGSize(width: witdh, height: height)).cgPath
    
    return roundedCorner
}
