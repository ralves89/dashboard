//
//  CircularProgressView.swift
//  Agente
//
//  Created by Renee Alves on 17/04/17.
//  Copyright © 2017 Renee Alves. All rights reserved.
//

import UIKit

class CircularProgressView: UIView {
    
    // MARK: VARIABLES
    
    fileprivate let progressLayer: CAShapeLayer     = CAShapeLayer()
    fileprivate let backgroundLayer: CAShapeLayer   = CAShapeLayer()
    fileprivate var progressLabel: SACountingLabel
    
    // MARK: UIVIEW DEFAULT
    
    required init?(coder aDecoder: NSCoder) {
        progressLabel = SACountingLabel()
        super.init(coder: aDecoder)
        self.backgroundColor = .clear
        createProgressLayer()
        createLabel()
    }
    
    override init(frame: CGRect) {
        progressLabel = SACountingLabel()
        super.init(frame: frame)
        self.backgroundColor = .clear
        createProgressLayer()
        createLabel()
    }
    
    // MARK: COUNTER LABEL CREATION
    
    fileprivate func createLabel() {
        
        let progressLabelFrame      = CGRect(x: 0.0, y: 0.0, width: frame.width, height: 60.0)
        
        progressLabel               = SACountingLabel(frame: progressLabelFrame)
        
        progressLabel.textColor     = UIColor.black
        progressLabel.textAlignment = .center
        progressLabel.font          = UIFont(name: "AvenirNext-Regular", size: 40.0)
        
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(progressLabel)
        
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: progressLabel, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: progressLabel, attribute: .centerY, multiplier: 1.0, constant: 0.0))
    }
    
    // MARK: PROGRESS LAYER CREATION
    
    fileprivate func createProgressLayer() {
        
        let startAngle  = CGFloat(3 * M_PI_2)
        let endAngle    = CGFloat(M_PI + M_PI_2 - 0.00001)
        let centerPoint = CGPoint(x: 49.0, y: frame.height/2)
        
        let bezierPath  = UIBezierPath(arcCenter:centerPoint, radius: 45.5, startAngle:startAngle, endAngle:endAngle, clockwise: true).cgPath
        
        progressLayer.path              = bezierPath
        progressLayer.backgroundColor   = UIColor.black.cgColor
        progressLayer.fillColor         = nil
        progressLayer.strokeColor       = UIColor.black.cgColor
        progressLayer.lineWidth         = 4.0
        progressLayer.strokeStart       = 0.0
        progressLayer.strokeEnd         = 1.0
        
        backgroundLayer.path            = bezierPath
        backgroundLayer.backgroundColor = UIColor.clear.cgColor
        backgroundLayer.fillColor       = nil
        backgroundLayer.strokeColor     = UIColor.black.cgColor
        backgroundLayer.lineWidth       = 4.0
        backgroundLayer.strokeStart     = 0.0
        backgroundLayer.strokeEnd       = 1.0
    }
    
    // MARK: GRADIENT MASK
    
    fileprivate func gradientMaskForBackgroundLayer() -> CAGradientLayer {
        
        let gradientLayer       = CAGradientLayer()
        
        gradientLayer.frame     = bounds
        gradientLayer.locations = [0.0, 1.0]
        
        let colorTop: AnyObject         = UIColor(red: 242.0/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0).cgColor
        let colorBottom: AnyObject      = UIColor(red: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0).cgColor
        let arrayOfColors: [AnyObject]  = [colorTop, colorBottom]
        
        gradientLayer.colors = arrayOfColors
        
        return gradientLayer
    }
    
    fileprivate func greenGradientMask() -> CAGradientLayer {
        
        let gradientLayer       = CAGradientLayer()
        
        gradientLayer.frame     = bounds
        gradientLayer.locations = [0.0,0.5,0.6, 1.0]
        
        let colorBottom: AnyObject      = UIColor(red: 159.0/255.0, green: 246.0/255.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        let colorTop: AnyObject         = UIColor(red: 48.0/255.0, green: 179.0/255.0, blue: 111.0/255.0, alpha: 1.0).cgColor
        let arrayOfColors: [AnyObject]  = [colorBottom, colorTop]
        
        gradientLayer.colors = arrayOfColors
        
        return gradientLayer
    }
    
    fileprivate func redGradientMask() -> CAGradientLayer {
        
        let gradientLayer       = CAGradientLayer()
        
        gradientLayer.frame     = bounds
        gradientLayer.locations = [0.0,0.5,0.6, 1.0]
        
        let colorBottom: AnyObject      = UIColor(red: 247.0/255.0, green: 146.0/255.0, blue: 159.0/255.0, alpha: 1.0).cgColor
        let colorTop: AnyObject         = UIColor(red: 233.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1.0).cgColor
        let arrayOfColors: [AnyObject]  = [colorBottom, colorTop]
        
        gradientLayer.colors = arrayOfColors
        
        return gradientLayer
    }
    
    fileprivate func yellowGradientMask() -> CAGradientLayer {
        
        let gradientLayer       = CAGradientLayer()
        
        gradientLayer.frame     = bounds
        gradientLayer.locations = [0.0,0.5,0.6, 1.0]
        
        let colorBottom: AnyObject      = UIColor(red: 249.0/255.0, green: 232.0/255.0, blue: 173.0/255.0, alpha: 1.0).cgColor
        let colorTop: AnyObject         = UIColor(red: 255.0/255.0, green: 199.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let arrayOfColors: [AnyObject]  = [colorBottom, colorTop]
        
        gradientLayer.colors = arrayOfColors
        
        return gradientLayer
    }
    
    // MARK: PROGRESS VIEW ANIMATION
    
    func animateProgressView(_ maxValue: CGFloat) {
        
        progressLayer.strokeEnd = 0.0
        
        var gradientMaskLayer = CAGradientLayer()
        
        if maxValue >= 0.0 && maxValue <= 0.750000 {
            
            gradientMaskLayer = redGradientMask()
            
        } else if maxValue > 0.750000 && maxValue <= 0.9000000 {
            
            gradientMaskLayer = yellowGradientMask()
            
        } else {
            
            gradientMaskLayer = greenGradientMask()
        }
        
        gradientMaskLayer.mask = progressLayer
        
        let backgroundMaskLayer = gradientMaskForBackgroundLayer()
        
        backgroundMaskLayer.mask = backgroundLayer
        
        layer.addSublayer(backgroundMaskLayer)
        layer.addSublayer(gradientMaskLayer)
        
        let animation                   = CABasicAnimation(keyPath: "strokeEnd")
        
        animation.fromValue             = CGFloat(0.0)
        animation.toValue               = CGFloat(maxValue)
        animation.duration              = 1.0
        animation.isRemovedOnCompletion   = false
        animation.isAdditive              = true
        animation.fillMode              = CAMediaTimingFillMode.forwards
        
        progressLayer.add(animation, forKey: "animateCircle")
        backgroundLayer.add(animation, forKey: "animateCircle")
        progressLabel.countFrom(0.0, to: (Float(maxValue) * 100), withDuration: 1.1, andAnimationType: .easeOut, andCountingType: .int)
    }
}
